﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class MeterEntity
    {
        public int MeterID { get; set; }
        public int MeterContractID { get; set; }
        public int AnnualConsumption { get; set; }
        public DateTime ContractEndDate { get; set; }
    }
}
