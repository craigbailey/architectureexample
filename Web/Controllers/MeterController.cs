﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Core.Models;
using Core.Services;
using Web.ViewModels;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/[controller]")]
    public class MeterController : Controller
    {
        private readonly IMapper _mapper;
        private readonly MeterService _service;

        public MeterController(IMapper mapper, MeterService service)
        {
            _mapper = mapper;
            _service = service;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            MeterModel meter = await _service.GetMeterByID(id, 0);

            var viewModel = _mapper.Map<MeterListViewModel>(meter);

            return new OkObjectResult(viewModel);
        }

    }
}
