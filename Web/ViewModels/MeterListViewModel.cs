﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class MeterListViewModel
    {
        public int MeterID { get; set; }
        public bool IsQuotable { get; set; }
    }
}
