﻿using System;
using System.Collections.Generic;
using System.Text;

using Core.Models;

namespace Core.Interfaces
{
    public interface IPricingEngine
    {
        List<PricingModel> GetPrices(string reference, int rateTypeID);
    }
}
