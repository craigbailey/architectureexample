﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;

using Core.Entities;
using Core.Interfaces;

namespace Core.Interfaces
{
    public interface IMeterRepository
    {
        Task<MeterEntity> GetMeterByID(int meterID);
        Task<MeterEntity> Save(MeterEntity meter);
    }
}
