﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class PricingModel
    {
        public string SupplierName { get; set; }
        public decimal Price { get; set; }
    }
}
