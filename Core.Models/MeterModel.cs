﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class MeterModel
    {
        public int MeterID { get; set; }
        public int MeterContractID { get; set; }
        public int AnnualConsumption { get; set; }
        public DateTime ContractEndDate { get; set; }

        public bool IsQuotable
        {
            get
            {
                return AnnualConsumption > 0 && ContractEndDate < DateTime.Now.AddYears(1);
            }
        }
    }
}
