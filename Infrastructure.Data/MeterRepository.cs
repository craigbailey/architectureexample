﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Text;
using Core.Entities;
using Core.Interfaces;

using Dapper;

namespace Infrastructure.Data
{
    public class MeterRepository : IMeterRepository
    {
        private IDbConnectionFactory _dbConnectionFactory;

        public MeterRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<MeterEntity> GetMeterByID(int meterID)
        {
            MeterEntity result = new MeterEntity();

            using (var conn = _dbConnectionFactory.CreateConnection())
            {
                result = await conn.QueryFirstOrDefaultAsync<MeterEntity>("uspMeter_GetByMeterID", new { @id = meterID }, commandType: CommandType.StoredProcedure);
            }

            return result;
        }

        public async Task<MeterEntity> Save(MeterEntity meter)
        {
            // Dynamic params etc here if needed.

            using (var conn = _dbConnectionFactory.CreateConnection())
            {
                int result = await conn.QueryFirstAsync<int>("uspMeter_Insert", null, commandType: CommandType.StoredProcedure);

                if (result > 0)
                    meter.MeterID = result;
            }

            return meter;
        }
    }
}
