﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Infrastructure.Data
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        // Wouldn't likely use this. Use IOptions Pattern for strongly typed config. Used as demo.
        private string _connectionString;

        public DbConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDbConnection CreateConnection()
        {
            var conn = new SqlConnection(_connectionString);
            return conn;
        }
    }
}
