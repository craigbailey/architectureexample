﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Core.Entities;
using Core.Models;
using Core.Interfaces;

namespace Core.Services
{
    public class MeterService
    {
        private IMeterRepository _meterRepo;
        
        public MeterService(IMeterRepository meterRepo)
        {
            _meterRepo = meterRepo;
        }

        public async Task<MeterModel> GetMeterByID(int meterID, int userID)
        {
            MeterEntity entity
        }

        public async Task<MeterModel> SaveMeter(MeterModel model)
        {
            MeterEntity entity = new MeterEntity();

            // Turn model into entity here.

            entity = await _meterRepo.Save(entity);

            if (entity.MeterID == 0)
            {
                // Problem saving. Do something here or pass down the chain?
            }

            model.MeterID = entity.MeterID;

            return model;
        }
    }
}
