﻿using System;
using System.Collections.Generic;

using Core.Interfaces;
using Core.Models;
using Infrastructure.PricingEngine.UDGroup.Models;

namespace Infrastructure.PricingEngine.UDGroup
{
    public class UDPricingEngine : IPricingEngine
    {
        public List<PricingModel> GetPrices(string reference, int rateTypeID)
        {
            // Build UD specific request
            UDPriceRequest request = new UDPriceRequest()
            {
                Reference = reference,
                RateTypeID = rateTypeID
            };

            // Make specific UD request (async HTTP call likely).
            // Turn UD results in our expected business result (PricingModel).

            return new List<PricingModel>();
        }
    }
}