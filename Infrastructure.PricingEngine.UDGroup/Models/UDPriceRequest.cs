﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.PricingEngine.UDGroup.Models
{
    public class UDPriceRequest
    {
        public string Reference { get; set; }
        public int RateTypeID { get; set; }
    }
}
